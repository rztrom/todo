//
//  main.m
//  ToDo
//
//  Created by Anders Zetterström on 2015-02-05.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
