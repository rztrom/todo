//
//  ToDoList.m
//  ToDo
//
//  Created by Anders Zetterström on 2015-02-05.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import "ToDoList.h"
#import "HandleTask.h"
#import "AddItem.h"
#import "Task.h"

@interface ToDoList ()

@end

@implementation ToDoList


-(void) viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    Task *task=[[Task alloc] init];
    self.toDoArray= [task toDoArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return self.toDoArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ToDoCell" forIndexPath:indexPath];
   
    cell.textLabel.text = self.toDoArray[indexPath.row];
    // Configure the cell...
    
    return cell;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"HandleTask"]){
        HandleTask *taskhandler = [segue destinationViewController];
        UITableViewCell *cell = sender;
        taskhandler.title= cell.textLabel.text;
        taskhandler.toDoArray= self.toDoArray;
    }
    else if ([segue.identifier isEqualToString:@"Add"]){

        AddItem *addItem = [segue destinationViewController];
        addItem.toDoArray= self.toDoArray;
    }
    else {
        NSLog(@"You forgot the to add the segue %@", segue);
    }
}


@end
