//
//  Task.m
//  ToDo
//
//  Created by Anders Zetterström on 2015-02-07.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import "Task.h"

@interface Task()

@end

@implementation Task


-(NSMutableArray*) toDoArray{
    if(!_toDoArray){
        _toDoArray= [@[@"Se färg torka", @"Tälja tandpetare", @"Svälja luft", @"Hålla andan", @"Vattna badkaret"] mutableCopy];
    }
    
    return _toDoArray;
}


@end
