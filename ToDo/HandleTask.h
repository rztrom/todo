//
//  HandleTask.h
//  ToDo
//
//  Created by Anders Zetterström on 2015-02-07.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HandleTask : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *toDoItem;

@property (nonatomic) NSMutableArray *toDoArray;
@end
