//
//  Task.h
//  ToDo
//
//  Created by Anders Zetterström on 2015-02-07.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject
@property (nonatomic) NSMutableArray *toDoArray;
@end
